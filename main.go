package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func getInput(prompt string, reader *bufio.Reader) (string, error) {
	fmt.Print(prompt + ": ")
	input, error := reader.ReadString('\n')

	return strings.TrimSpace(input), error
}

func createBill() bill {
	// buffer io, read from os standard input
	reader := bufio.NewReader(os.Stdin)

	name, _ := getInput("Enter bill name", reader)
	myBill := newBill(name)
	fmt.Println("Bill created: ", myBill.name)

	return myBill
}

func promptOptions(bill bill) {
	reader := bufio.NewReader(os.Stdin)

	option, _ := getInput("Choose option (a - add item, t - add tip, s - save bill) ", reader)
	fmt.Println("You choose: ", option)

	switch option {
	case "a":
		item, _ := getInput("Item name: ", reader)
		price, _ := getInput("Price: ", reader)

		// strconv - string converter
		priceFloat, err := strconv.ParseFloat(price, 64)

		if err != nil {
			fmt.Println("Invalid price")
			promptOptions(bill)
		}

		bill.addItem(item, priceFloat)
		fmt.Println("Item added: ", item)
		promptOptions(bill)
	case "t":
		tip, _ := getInput("Tip: ", reader)

		// strconv - string converter
		tipFloat, err := strconv.ParseFloat(tip, 64)

		if err != nil {
			fmt.Println("Invalid tip")
			promptOptions(bill)
		}

		bill.addTip(tipFloat)

		fmt.Println("Tip added: ", tip)
		promptOptions(bill)
	case "s":
		bill.save()
	default:
		fmt.Println("Invalid option")
	}
}

func (bill *bill) save() {
	fmt.Println("Saving bill: ", bill.name)

	// convert to byte
	data := []byte(bill.format())

	err := os.WriteFile("bills/"+bill.name+".txt", data, 0644)

	if err != nil {
		panic(err)
	}

	fmt.Println("Bil saved")
}

func main() {
	myBill := createBill()
	promptOptions(myBill)

	fmt.Println(myBill.format())
}
