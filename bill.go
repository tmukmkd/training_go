package main

import (
	"fmt"
)

// struct - structure
type bill struct {
	name  string
	items map[string]float64
	tip   float64
}

// generate new bill
func newBill(name string) bill {
	newBill := bill{
		name:  name,
		items: map[string]float64{},
		tip:   0,
	}

	return newBill
}

// format the bill
func (rawBill bill) format() string {
	formattedString := "Bill breakdown: \n"
	var total float64 = 0

	// list items
	// -25 = add 25 char as padding-right
	// 25 = add 25 char as padding-left
	for key, value := range rawBill.items {
		formattedString += fmt.Sprintf("%-25v: ... $%0.2f\n", key, value)
		total += value
	}

	// tip
	formattedString += fmt.Sprintf("%-25v: ... $%0.2f\n", "Tip", rawBill.tip)

	// total
	formattedString += fmt.Sprintf("%-25v: ... $%0.2f\n", "Total", total+rawBill.tip)

	return formattedString
}

// update tip
func (rawBill *bill) addTip(tip float64) {
	// no need to refer to pointer because of struct
	//(*rawBill).tip = tip
	rawBill.tip = tip
}

// add item to the bill
func (rawBill *bill) addItem(name string, price float64) {
	rawBill.items[name] = price
}
